// console.log("Hello World!")

/*
	While Loop

		Syntac:
			while(expression/condition) {
					statement
			}


*/

let count = 5

while (count!== 0) {
	console.log("While: " + count)
	count-- //coun -= 1 // count = count - 1
}

/*
	Mini Activity:
		the while loop should only display the numbers 1-5
		Correct the following loop
		
		let x = 0

		while (x < 1) {
			console.log(x)
			x--;
		}

*/


let x = 0
x = 1

		while (x <= 5) {
			console.log(x)
			x++;
		}


/*
	Do While Loop
		a do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.

		Syntax:
			do {
				statement
			} while (expression/condition)

*/

// let number = Number(prompt("Give me a number: "))

// do {

// 	console.log("Do while: " + number)
// 	// number = number + 1
// 	number += 1
// } while(number < 10)


/*
	For Loop
		- more flexible than the while loop and do-while loop

	Syntax:
		fo (initialization; expression/condition; final expression) {
			statement
		}

*/

for(let count = 0; count <= 20; count++) {
	console.log("For count:" + count)
}


let myString = "rupert ramos"
console.log(myString.length)

console.log(myString[0])
console.log(myString[1])
console.log(myString[11])

for (let x = 0; x < myString.length; x++) {
	console.log(myString[x])
}

let myName = "DARIEN"

for(let d = 0; d < myName.length; d++) {

	if(
		myName[d].toLowerCase() == "a" ||
		myName[d].toLowerCase() == "e" ||
		myName[d].toLowerCase() == "i" ||
		myName[d].toLowerCase() == "o" ||
		myName[d].toLowerCase() == "u" 
	) {
		console.log("Vowel")
	} else {
		console.log(myName[d])
	}
}

// NEED HELP HERE

// Continue and Break Statements

/*
	"Continue" statement allows the code  to go to the next iteration of the loop without finihsing the execution of all the statements in the code block

*/


for (let count = 0; count <=20; count++) {

	// console.log("hello world " + count)

	if(count % 2 === 0) {
		// console.log("Even Number")
		continue;
	}
	console.log("Continue and Break: " + count)

	if(count > 10) {
		break;
	}
		
}

let name = "Alexandro"

for (let i = 0; i < name.length; i++) {
	console.log(name[i]);

	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the iteration")
		continue;
	}
	// console.log("Hello")

	if(name[i].toLowerCase() === "d") {
		console.log("Continue to the iteration")
		break;
	}


}
